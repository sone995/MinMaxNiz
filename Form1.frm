VERSION 5.00
Begin VB.Form MaxMinNiz 
   Caption         =   "Form1"
   ClientHeight    =   5130
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   7905
   LinkTopic       =   "Form1"
   ScaleHeight     =   5130
   ScaleWidth      =   7905
   StartUpPosition =   3  'Windows Default
   Begin VB.TextBox UnNiz 
      Height          =   615
      Left            =   0
      TabIndex        =   9
      Top             =   2880
      Width           =   7815
   End
   Begin VB.TextBox MaxN 
      Height          =   615
      Left            =   3360
      TabIndex        =   7
      Top             =   840
      Width           =   1335
   End
   Begin VB.TextBox MinN 
      Height          =   615
      Left            =   4800
      TabIndex        =   6
      Top             =   840
      Width           =   1335
   End
   Begin VB.CommandButton EndNiz 
      Caption         =   "Kraj"
      Height          =   495
      Left            =   6240
      TabIndex        =   5
      Top             =   240
      Width           =   1335
   End
   Begin VB.CommandButton MinNiz 
      Caption         =   "Minimalni clan Niza"
      Height          =   495
      Left            =   4800
      TabIndex        =   4
      Top             =   240
      Width           =   1335
   End
   Begin VB.CommandButton MaxNiz 
      Caption         =   "Najveci clan Niza"
      Height          =   495
      Left            =   3360
      TabIndex        =   3
      Top             =   240
      Width           =   1335
   End
   Begin VB.CommandButton UNiz 
      Caption         =   "UnosNiza"
      Height          =   495
      Left            =   1920
      TabIndex        =   2
      Top             =   240
      Width           =   1335
   End
   Begin VB.TextBox TxtElnNiz 
      Height          =   615
      Left            =   120
      TabIndex        =   1
      Top             =   840
      Width           =   1695
   End
   Begin VB.Label Label2 
      Caption         =   "Uneseni Niz"
      Height          =   375
      Left            =   120
      TabIndex        =   8
      Top             =   2280
      Width           =   1575
   End
   Begin VB.Label Label1 
      Caption         =   "Unesi broj elemenata niza"
      Height          =   495
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   1575
   End
End
Attribute VB_Name = "MaxMinNiz"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub EndNiz_Click()
End
End Sub
Private Sub MaxNiz_Click()
MaxN.Text = Maksimum(ByVal ElnNiz)
End Sub
Private Sub MinNiz_Click()
MinN.Text = Minimum(ByVal ElnNiz)
End Sub
Private Sub UNiz_Click()
Dim Poruka1, ii, SNiz1 As String
Dim i As Integer

ElnNiz = Val(TxtElnNiz.Text)
If ElnNiz < 101 Then
   UnNiz.Text = ""
   For i = 1 To ElnNiz
       ii = Str(i)
       Poruka1 = " Unesi  " + ii + "-ti clan Niza"
       aa = InputBox(Poruka1, "Unos clanova niza")
       
       Niz1(i) = aa
       SNiz1 = Str(aa)
       If i = 1 Then
          UnNiz.Text = SNiz1
   Else
     UnNiz.Text = UnNiz.Text + "; " + SNiz1
   End If
  Next i
 Else
   MsgBox ("Vas niz je definisan da se moze uneti maksimalno 100 clanova niza")
End If
End Sub
Private Function Maksimum(ByVal N As Integer) As Integer
Dim Max, i As Integer
Max = Niz1(1)
For i = 2 To N
    If Niz1(i) > Max Then
       Max = Niz1(i)
    End If
Next i
Maksimum = Max
End Function
Private Function Minimum(ByVal N As Integer) As Integer
Dim Max, i As Integer
Min = Niz1(1)
For i = 2 To N
    If Niz1(i) < Min Then
       Min = Niz1(i)
    End If
Next i
Minimum = Min
End Function
